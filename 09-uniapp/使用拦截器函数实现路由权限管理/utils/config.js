export default {
	// 白名单，不需要登录也可以访问的页面
	whiteList: [
		'/',
		'/pages/index/index',
		'/pages/test1/test1',
		"/pages/login/login"
	],
	// 登录页
	loginPage:'/pages/login/login'
}
