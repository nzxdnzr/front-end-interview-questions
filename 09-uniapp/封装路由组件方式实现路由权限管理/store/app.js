const app = {
  namespaced: true,
  state: {
    token: uni.getStorageSync("token") || "",
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
      uni.setStorageSync("token", token);
    },
    logout(state) {
      state.token = "";
      uni.setStorageSync("token", state.token);
    },
  },
};

export default app;
