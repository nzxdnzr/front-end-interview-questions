import App from "./App";
import Vue from "vue";

import RouterLink from "@/components/RouterLink.vue";
import navigate from "@/utils/router.js";
import store from "@/store/index.js";
Vue.prototype.$navigate = navigate;
Vue.prototype.$store = store;

Vue.config.productionTip = false;
App.mpType = "app";

Vue.component(RouterLink.name, RouterLink);
const app = new Vue({
  ...App,
});
app.$mount();
