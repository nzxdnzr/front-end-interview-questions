
import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "BeforeEach",
    component: () => import("../views/BeforeEach.vue"),
  },
  {
    path: "/beforeenter",
    name: "BeforeEnter",
    component: () => import("../views/BeforeEnter.vue"),
    meta: {
      isAuth: true
    }
    // beforeEnter: (to, from) => {
    //   console.log('要去哪儿', to);
    //   console.log('来自哪儿', from);
    // }  
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

// 全局前置守卫：初始化时执行、每次路由切换前执行
// 可通过给路由添加 meta 来判断是否需要进行判断
router.beforeEach((to, from, next) => {
  console.log('要去哪儿', to);
  console.log('来自哪儿', from);
  if (to.meta.isAuth) {
    if (localStorage.getItem('token')) {
      console.log('你可以过去');
      next()
    } else {
      alert('此路不通')
    }
  }else{
    next()
  }
})

//全局后置守卫：初始化时执行、每次路由切换后执行
// 应用场景：跳转到点击的路由组件后，再进行一些调整，比如给路由组件起标题
router.afterEach(() => {
  document.title = '路由守卫'
})
export default router;
