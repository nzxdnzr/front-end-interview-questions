import { createRouter, createWebHashHistory } from "vue-router";

const Admin = () => import("~/layouts/admin.vue");
const Index = () => import("~/pages/index.vue");
const Login = () => import("~/pages/login.vue");
const NotFound = () => import("~/pages/404.vue");
const GoodList = () => import("~/pages/goods/list.vue");
const CategoryList = () => import("~/pages/category/list.vue");
// 默认路由 所有用户共享
const routes = [
  {
    path: "/",
    name: "admin",
    component: Admin,
  },
  {
    path: "/login",
    component: Login,
    meta: {
      title: "登录页",
    },
  },
  { path: "/:pathMatch(.*)*", name: "NotFound", component: NotFound },
];

// 动态路由，用于匹配菜单动态添加路由
const asyncRoutes = [
  {
    path: "/",
    name: "/",
    component: Index,
    meta: {
      title: "后台首页",
    },
  },
  {
    path: "/goods/list",
    name: "/goods/list",
    component: GoodList,
    meta: {
      title: "商品管理",
    },
  },
  {
    path: "/category/list",
    name: "/category/list",
    component: CategoryList,
    meta: {
      title: "分类列表",
    },
  },
];
const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export function addRoutes(menus) {
  // 表示当次是否添加有新路由
  let hasNewRoutes = false;

  const findAndAddRoutesByMenus = (frontRoutes) => {
    /*
      <menuItem>{
        frontpath
        child: [<menuItem>{
          frontpath
          child: [<menuItem>{}]
        }]
      }
    */

    frontRoutes.forEach((frontRoutesItem) => {
      // 双重循环查找本地路由列表中符合后台返回路由要求的对象
      let asyncRoutesItemToAdd = asyncRoutes.find(
        // menuItem.frontpath 后台为前端指定需要展示的路径    front => 前端
        (asyncRoutesItem) => asyncRoutesItem.path == frontRoutesItem.frontpath
      );

      // 如果本地列表中存在并且本地初始化路由中还没有添加这个路径
      // 正常添加
      if (asyncRoutesItemToAdd && !router.hasRoute(asyncRoutesItemToAdd.path)) {
        // 添加到 name 为 admin 的路由对象 children 中
        // 如果只传递 item 路由对象,就添加到根路由数组中
        // 筛选 Layout 组件内 router-view 可以显示的内容
        router.addRoute("admin", asyncRoutesItemToAdd);

        // 更新标记,表示有新路由添加
        hasNewRoutes = true;
      }

      // 递归处理 child 内部的 frontRoutesItem
      if (frontRoutesItem.child && frontRoutesItem.child.length > 0) {
        findAndAddRoutesByMenus(frontRoutesItem.child);
      }
    });
  };

  // 变量后台返回的基础数据
  findAndAddRoutesByMenus(menus);

  // 向外传递表示是否已经添加新路由
  return hasNewRoutes;
}

export default router;
