const user = {
  namespaced: true,
  state: {
    username: 'Mike'
  },
  mutations: {
    changeUsername(state, payload) {
      console.log(payload);
      state.username = payload
    }
  },
  actions: {
    asyncChangeUsername({ commit }, payload) {
      setTimeout(() => {
        commit('changeUsername', payload.username)
      }, 2000)
    }
  },
}

export default user