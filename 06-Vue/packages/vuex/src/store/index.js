import { createStore } from 'vuex'
import persistedState from 'vuex-persistedstate'

import user from './user'

const store = createStore({
  // 定义vuex保存的值
  state: {
    name: 'rose'
  },
  // 在state中的数据的基础上，进一步对数据进行加工得到新数据
  // 也就是通过函数 return 出你需要的 state 里的数据
  getters: {
    getName(state) {
      return state
    }
  },
  // 修改vuex存的值，同步
  mutations: {
    changeName(state, payload) {
      state.name = payload
    }
  },
  // 修改vuex存的值，异步
  actions: {
    asyncChangeName({ commit }, payload) {
      setTimeout(() => {
        commit('changeName', payload.name)
      }, 2000)
    }
  },
  // 将拆分后的模块写在这里
  modules: {
    user
  },
  // 添加状态持久化，刷新页面数据也在
  plugins: [persistedState()]
})

export default store