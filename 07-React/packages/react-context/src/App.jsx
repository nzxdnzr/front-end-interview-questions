import { useState, useContext, createContext } from "react";
import "./App.css";
// 创建上下文对象
const Context = createContext();
const { Provider } = Context;

const A = () => {
  const [username, setUsername] = useState("tom");
  return (
    <div className="parent">
      <h3>我是A组件</h3>
      <h5>我的用户名是：{username}</h5>
      <Provider value={{ username, setUsername }}>
        <B></B>
      </Provider>
    </div>
  );
};

const B = () => {
  const content = useContext(Context);
  return (
    <div className="child">
      <h3>我是B组件</h3>
      <h5>我从A组件收到的用户名是：{content.username}</h5>
      <button
        onClick={() => {
          content.setUsername("jack");
        }}
      >
        修改用户名
      </button>
      <C></C>
    </div>
  );
};

const C = () => {
  const content = useContext(Context);
  console.log(content);
  return (
    <div className="grand">
      <h3>我是C组件</h3>
      <h5>我从A组件收到的用户名是：{content.username}</h5>

      {/* <Context.Consumer>
        {(value) => {
          return <h5>我从A组件收到的用户名是：{value.username}</h5>;
        }}
      </Context.Consumer> */}
      <D></D>
    </div>
  );
};
const D = () => {
  return (
    <div style={{ backgroundColor: "#fff" }}>
      <p>
        Context 提供了一个无需为每层组件手动添加
        props，就能在组件树间进行数据传递的方法。
      </p>
      <p>使用context可以避免在通过中间元素传递props</p>
    </div>
  );
};

export default A;
