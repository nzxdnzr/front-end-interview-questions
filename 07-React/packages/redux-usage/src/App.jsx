import { useEffect, useState } from "react";

let App = (props) => {
  let { store } = props;

  // 获取 store 中的状态作为组件内初始状态
  const [count, setCount] = useState(() => {
    return store.getState().count;
  });

  useEffect(() => {
    // 对 store 中的状态进行订阅，当状态被修改时触发订阅函数，同步修改组件内状态
    store.subscribe(() => {
      setCount(() => {
        // 注意，在更新组件状态时，仍需要获取 store 内状态进行使用
        return store.getState().count;
      });
    });
    // 参数 2 传递空数组，仅订阅一次即可
  }, []);

  const change = () => {
    // 使用专用方法修改 store 中的状态
    store.dispatch({ type: "add", payload: 1 });
  };

  return (
    <div className="App">
      <p>{count}</p>
      <button onClick={change}>增加</button>
    </div>
  );
};

export default App;
