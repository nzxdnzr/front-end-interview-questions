const initState = { count: 0 };

export default function reducer(state = initState, action) {
  let { type, payload } = action;

  switch (type) {
    case "add": {
      // 在状态发生变化后，必须返回新对象，满足纯函数要求
      // 同样满足 react 的数据变化侦听原则
      return {
        ...state,
        count: state.count + payload,
      };
    }
    // 在没有对应的 type 匹配时，返回函数原始状态
    default: {
      return state;
    }
  }
}
