/**
 * Redux v4.2 开始，废弃原 createStore，改为 legacy_createStore
 */
import { legacy_createStore as createStore } from "redux";
import reducer from "./reducer";

export default createStore(reducer);
