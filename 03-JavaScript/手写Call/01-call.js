// 1.默认绑定
// 当函数调用类型为：独立函数调用时，函数的 this 为默认绑定，指向全局变量；在严格模式下，this 将绑定到 underfined

// 调用在全局
// 没有被其他对象包裹
// 这就叫做独立函数
function foo() {
  console.log(this.a); // underfined
}
foo();

// 2 隐式绑定
// 当函数的 调用位置 有上下文对象时，或者说函数在被调用时 被某个对象拥有或者包含时，隐式绑定规则就会把函数调用中的 this 绑定到这个上下文对象。
// 如下，foo 在调用时 this 便被隐式绑定到了 obj 上
function foo() {
  console.log(this.a);
}
const obj = { a: 2, foo };

obj.foo(); // 2

// 显示绑定
// 使用 call apply和bind显示的绑定函数调用时的 this 指向
