let t = null;
//  懒加载函数
// $是query的缩写
// $.fn是指jquery的命名空间，加上fn的方法及属性，会对jquery实例有效
// 如扩展$.fn.lazyLoad() 就是对jquery() 扩展了一个lazyLoad()方法，在jquery实例中调用这个方法 例如$("img").lazyLoad()
$.fn.lazyLoad = function () {
  let _this = this;
  function lazyLoad() {
    if (t) clearTimeout(t);
    t = setTimeout(() => {
      // 使用 || 是为了解决浏览器兼容性问题
      // 对于Internet Explorer、Chrome、Firefox、Opera 以及 Safari： 使用window.innerHeight
      // 对于 Internet Explorer 8、7、6、5： 使用document.documentElement.clientHeight
      var windowH = window.innerHeight || document.documentElement.clientHeight;

      // 1.各浏览器下 scrollTop的差异:
      // IE6/7/8：
      // 可以使用 document.documentElement.scrollTop；
      // IE9及以上：
      // 可以使用window.pageYOffset或者document.documentElement.scrollTop
      // Safari:
      // safari： window.pageYOffset 与document.body.scrollTop都可以；
      // Firefox:
      // 火狐等等相对标准些的浏览器就省心多了，直接用window.pageYOffset 或者 document.documentElement.scrollTop ；
      // Chrome：
      // 谷歌浏览器只认识document.body.scrollTop;

      // 可视窗口的内容高 或者 文档html节点的clientHeight
      // 文档body节点的 与文档最顶部（纵坐标）的距离 或者 文档html节点与文档最顶部的距离
      var scrollH =
        document.body.scrollTop || document.documentElement.scrollTop;

      var showH = windowH + scrollH;
      let imgs = _this;
      for (let i = 0; i < imgs.length; i++) {
        // 当前图片的上边框与父元素上边框距离
        if (imgs[i].offsetTop < showH + 100) {
          imgs[i].src = imgs[i].getAttribute("data-src");
        }
      }
    }, 500);
  }
  lazyLoad();
  window.onscroll = lazyLoad;
};
